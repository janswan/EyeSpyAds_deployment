# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, request ,response
from datetime import datetime
from django.db import models
from .models import Ads, User

from django.template.response import TemplateResponse
#HttpResponse("<h1>YES Jannie , GO Jannie</h1>")
# Create your views here.
def index(request):
    myFilter = Ads.objects.get(ID = 1)
    print (myFilter.title)
    return TemplateResponse(request, 'index.html', {"ad":myFilter} )



def testStatic(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)

def templateRespon(request):
    return TemplateResponse(request, 'index.html', {})