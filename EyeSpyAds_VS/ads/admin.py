# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Ads,User, Stat,Activity,BusinessHours
# Register your models here.
admin.site.register(Ads)
admin.site.register(Activity)
admin.site.register(User)
admin.site.register(Stat)
admin.site.register(BusinessHours)
