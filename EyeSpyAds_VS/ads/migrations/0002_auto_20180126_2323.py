# Generated by Django 2.0.1 on 2018-01-26 21:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessHours',
            fields=[
                ('id', models.CharField(max_length=6, primary_key=True, serialize=False)),
                ('adID', models.CharField(blank=True, max_length=6)),
                ('mon_start', models.TimeField(blank=True)),
                ('mon_end', models.TimeField(blank=True)),
                ('tue_start', models.TimeField(blank=True)),
                ('tue_end', models.TimeField(blank=True)),
                ('wed_start', models.TimeField(blank=True)),
                ('wed_end', models.TimeField(blank=True)),
                ('thurs_start', models.TimeField(blank=True)),
                ('thurs_end', models.TimeField(blank=True)),
                ('fri_start', models.TimeField(blank=True)),
                ('fri_end', models.TimeField(blank=True)),
                ('sat_start', models.TimeField(blank=True)),
                ('sat_end', models.TimeField(blank=True)),
            ],
        ),
        migrations.RenameField(
            model_name='user',
            old_name='name',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='surname',
            new_name='last_name',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='fb_link',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o1_link',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o1name',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o2_link',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o2name',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o3_link',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='o3name',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='photo4',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='photo5',
        ),
        migrations.RemoveField(
            model_name='ads',
            name='www_link',
        ),
        migrations.AddField(
            model_name='ads',
            name='email',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AddField(
            model_name='ads',
            name='fb',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AddField(
            model_name='ads',
            name='gpr_longitude',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='ads',
            name='gps_latitude',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='ads',
            name='instagram',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AddField(
            model_name='ads',
            name='twitter',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AddField(
            model_name='ads',
            name='website',
            field=models.CharField(blank=True, max_length=70),
        ),
        migrations.AlterField(
            model_name='ads',
            name='category',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AlterField(
            model_name='ads',
            name='description',
            field=models.TextField(blank=True, max_length=900),
        ),
        migrations.AlterField(
            model_name='ads',
            name='logo_filename',
            field=models.CharField(blank=True, max_length=300),
        ),
        migrations.AlterField(
            model_name='ads',
            name='number',
            field=models.CharField(blank=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='ads',
            name='photo1',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AlterField(
            model_name='ads',
            name='photo2',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AlterField(
            model_name='ads',
            name='photo3',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AlterField(
            model_name='ads',
            name='physical_address',
            field=models.TextField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='ads',
            name='tags',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='ads',
            name='business_hours',
            field=models.ForeignKey(blank=True, default=123456, on_delete=django.db.models.deletion.CASCADE, to='ads.BusinessHours'),
            preserve_default=False,
        ),
    ]
