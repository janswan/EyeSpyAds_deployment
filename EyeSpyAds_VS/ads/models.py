# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
#global Var -> sal dit graag eerder in  setting declaer

#advert_data_url = "/advert_data/"
class BusinessHours(models.Model):
    id = models.CharField(max_length=6, primary_key= True)
    adID = models.CharField(max_length=6, blank=True) # einklik redundant maar vir display purpoese
    mon_start = models.TimeField(auto_now=False, blank=True)
    mon_end = models.TimeField(auto_now=False, blank=True)
    tue_start = models.TimeField(auto_now=False, blank=True)
    tue_end = models.TimeField(auto_now=False, blank=True)
    wed_start = models.TimeField(auto_now=False, blank=True)
    wed_end = models.TimeField(auto_now=False, blank=True)
    thurs_start = models.TimeField(auto_now=False, blank=True)
    thurs_end = models.TimeField(auto_now=False, blank=True)
    fri_start = models.TimeField(auto_now=False, blank=True)
    fri_end = models.TimeField(auto_now=False, blank=True)
    sat_start = models.TimeField(auto_now=False, blank=True)
    sat_end = models.TimeField(auto_now=False, blank=True)


class Ads(models.Model):
    ID = models.CharField(max_length=6,primary_key=True)
    title = models.CharField(max_length=70)
    description = models.TextField(max_length=900,blank=True)
    tags = models.CharField(max_length=500,blank=True)
    category = models.CharField(max_length=500,blank=True)
    website = models.CharField(max_length=70, blank=True)
    email = models.CharField(max_length=70,blank=True)
    fb = models.CharField(max_length=70,blank=True)
    twitter = models.CharField(max_length=70,blank=True)
    instagram = models.CharField(max_length=70,blank=True)
    number  = models.CharField(max_length=10,blank=True)
    physical_address = models.TextField(max_length=100,blank=True)
    gps_latitude = models.FloatField(default=0)
    gpr_longitude = models.FloatField(default=0)
    #TODO add Business hours -> textfiled, or more specified date multiple day conversions (kan dan se OPEN Now, or closed)
    #moontlik nog n table maak dan met n common field vir die verskillende dae -> dink bietjie hieroor vir eers TextField
    business_hours = models.ForeignKey(BusinessHours, on_delete=models.CASCADE, blank=True)
    logo_filename = models.CharField(max_length=300,blank=True)
    num_photos = models.IntegerField()
    photo1 = models.CharField(max_length=150,blank=True)
    photo2 = models.CharField(max_length=150,blank=True)
    photo3 = models.CharField(max_length=150,blank=True)
    enable = models.BooleanField()


    def __str__(self):
        return self.title

    # @brief
    #       Returns logo path in the static folder
    # returns advert/addTitle/logofileName.png
    def getLogo(self):
        output = settings.STATIC_URL
        output = output + "advert_data/" + self.title + "_" + self.ID + "/" + self.logo_filename
        return output

    #TODO -> lowPriority ,easy photo and logo naming convention without typing it (auto detect and more and renaim)
    # @brief
    #   returs a list of photo paths in the static folder , will
    #   use {% static (and this)[i] %} in template src=
    #
    #  @auther J. Swanepoel
    def getPhotos(self):
        preamble = settings.STATIC_URL+"advert_data/"+ self.title+"_" +self.ID+"/"
        photos = []
        if self.photo1:
            photos.append(preamble+self.photo1)
        if self.photo2:
            photos.append(preamble+self.photo2)
        if self.photo3:
            photos.append(preamble+self.photo3)
        return photos

    def getDescription(self):
        return self.description.splitlines()

    def getTitle(self):
        return self.title.capitalize()

    def getTags(self):
        return self.tags.split()

    def getNumber(self):
        #if (self.number.__len__ == 10): #defualt ZA number
        if(len(self.number)==10):
            number = self.number
            numberFormated  =number[0]+number[1]+number[2] +" "+number[3]+number[4]+number[5]+" "+number[6]+number[7]+number[8]+number[9]
        else:
            numberFormated = self.number
        return numberFormated

    #def generateID(self):
        #somecode here 6Chars

    def physical_address_lines(self):
        return self.physical_address.splitlines()

    def getBusinessHours(self): #todo objects to string
        temp = "underConstrction\n:)\nPlease be pationt"
        return temp.splitlines() # self.business_hours.splitlines()




class User(models.Model):
    id = models.CharField(max_length=6, primary_key= True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=30)

    def getFullName(self):
        return self.name + self.surname

class Activity(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    add_id = models.ForeignKey(Ads,on_delete=models.CASCADE)
    activation_date = models.DateField()
    duration = models.IntegerField()

class Stat(models.Model):
    add_id = models.ForeignKey(Ads, on_delete=models.CASCADE)
    content_type = models.CharField(max_length=6)

# EK dink dalk ek moet die fotos rename as addName_1 addName_logo dan kan ek dit hardcode